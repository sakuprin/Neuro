package edu.sakuprin.neuro

import edu.sakuprin.neuro.utils.infix.getIntelligentNeuralNetwork
import edu.sakuprin.neuro.utils.infix.result

fun main(args: Array<String>) {
    val network = getIntelligentNeuralNetwork(3, 9, 1, 10000)
    result(network, doubleArrayOf(0.0, 0.0, 0.0))
    result(network, doubleArrayOf(0.0, 1.0, 0.0))
    result(network, doubleArrayOf(0.0, 0.0, 1.0))
    result(network, doubleArrayOf(0.0, 1.0, 1.0))
    result(network, doubleArrayOf(1.0, 0.0, 0.0))
    result(network, doubleArrayOf(1.0, 1.0, 0.0))
    result(network, doubleArrayOf(1.0, 0.0, 1.0))
    result(network, doubleArrayOf(1.0, 1.0, 1.0))
}