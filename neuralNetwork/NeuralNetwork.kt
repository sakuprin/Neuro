package edu.sakuprin.neuro.neuralNetwork


import edu.sakuprin.neuro.neuron.Neuron

interface NeuralNetwork {
    fun calculate(inputValues: DoubleArray): DoubleArray
    val layers: List<List<Neuron>>
}
