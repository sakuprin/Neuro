package edu.sakuprin.neuro.neuralNetwork.impl

import edu.sakuprin.neuro.activationFunctions.impl.ActivationFunctionImpl
import edu.sakuprin.neuro.neuralNetwork.NeuralNetwork
import edu.sakuprin.neuro.neuron.Neuron
import edu.sakuprin.neuro.neuron.impl.InputNeuron
import edu.sakuprin.neuro.neuron.impl.Link
import edu.sakuprin.neuro.neuron.impl.NeuronImpl
import edu.sakuprin.neuro.utils.infix.recalculateOutputValues
import edu.sakuprin.neuro.utils.infix.setInputValues
import java.util.*

class NeuralNetworkImpl : NeuralNetwork {
    val random = Random()
    val inputLayer = ArrayList<Neuron>()
    val middleLayer = ArrayList<Neuron>()
    val outputLayer = ArrayList<Neuron>()
    override val layers = Arrays.asList<List<Neuron>>(middleLayer, outputLayer)

    fun init(inputCount: Int, middleCount: Int, outputCount: Int): NeuralNetwork {
        for (i in 1..inputCount) {
            val neuron = InputNeuron()
            neuron.setActivationFunction(ActivationFunctionImpl)
            inputLayer.add(neuron)
        }
        config(this.inputLayer, this.middleLayer, middleCount)
        config(middleLayer, outputLayer, outputCount)
        return this
    }

    private fun config(neurons: List<Neuron>, resultNeurons: MutableList<Neuron>, count: Int) {
        for (i in 1..count) {
            val outputNeuron = NeuronImpl()
            outputNeuron.setActivationFunction(ActivationFunctionImpl)
            for (inputNeuron in neurons) {
                val link = linkNeurons(inputNeuron, outputNeuron)
                link.weight = random.nextDouble()
            }
            resultNeurons.add(outputNeuron)
        }
    }

    private fun linkNeurons(inputNeuron: Neuron, outputNeuron: Neuron): Link {
        val link = Link()
        inputNeuron.addOutputLink(link)
        link.inputNeuron = inputNeuron
        outputNeuron.addInputLink(link)
        link.outputNeuron = outputNeuron
        return link
    }

    override fun calculate(inputValues: DoubleArray): DoubleArray {
        if (inputValues.size != inputLayer.size) {
            throw IllegalArgumentException("Count of input values must be equals ${inputLayer.size}")
        }
        this setInputValues inputValues
        val result = this recalculateOutputValues outputLayer
        return result
    }
}


