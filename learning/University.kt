package edu.sakuprin.neuro.learning


import edu.sakuprin.neuro.neuralNetwork.NeuralNetwork

interface University {
    fun learning(neuralNetwork: NeuralNetwork, inputValues: DoubleArray, expectedOutputValues: DoubleArray)
}
