package edu.sakuprin.neuro.learning.impl

import edu.sakuprin.neuro.learning.University
import edu.sakuprin.neuro.neuralNetwork.NeuralNetwork
import edu.sakuprin.neuro.neuron.Neuron
import java.util.*

class UniversityImpl : University {

    val neuronErrorsContainer = HashMap<Neuron, Double>()

    override fun learning(neuralNetwork: NeuralNetwork, inputValues: DoubleArray, expectedOutputValues: DoubleArray) {
        val outputValues = neuralNetwork.calculate(inputValues)
        for (index in neuralNetwork.layers[1].indices) {
            val outNeuron = neuralNetwork.layers[1][index]
            val value = (expectedOutputValues[index] - outputValues[index]) * outNeuron.getDerivativeValue()
            neuronErrorsContainer.put(outNeuron, value)
            recalculateWeight(outNeuron, neuronErrorsContainer.get(outNeuron) as Double)
        }
        for (currentNeuron in neuralNetwork.layers[0]) {
            var delta = 0.0
            for (link in currentNeuron.getOutputLinks()) {
                delta += link.weight * neuronErrorsContainer[link.outputNeuron]!!
            }
            recalculateWeight(currentNeuron, delta * currentNeuron.getDerivativeValue())
        }
    }

    private fun recalculateWeight(currentNeuron: Neuron, error: Double) {
        for (inputLink in currentNeuron.getInputLinks()) {
            val weightDelta = error * inputLink.inputNeuron!!.recalculateOutputValue()
            val weight = inputLink.weight + weightDelta
            inputLink.weight = weight
        }
    }
}