package edu.sakuprin.neuro.activationFunctions.impl

import edu.sakuprin.neuro.activationFunctions.ActivationFunction


object ActivationFunctionImpl : ActivationFunction {
    override fun process(value: Double): Double {
        return 1 / (1 + Math.pow(Math.E, -value))
    }

    override fun recalculate(value: Double): Double {
        val processValue = process(value)
        return processValue * (1 - processValue)
    }
}
