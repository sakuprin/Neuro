package edu.sakuprin.neuro.activationFunctions


interface ActivationFunction {
    fun process(value: Double): Double
    fun recalculate(value: Double): Double
}