package edu.sakuprin.neuro.neuron

import edu.sakuprin.neuro.activationFunctions.ActivationFunction
import edu.sakuprin.neuro.neuron.impl.Link

interface Neuron {

    fun recalculateOutputValue(): Double

    fun getDerivativeValue(): Double

    fun setActivationFunction(activationFunction: ActivationFunction)

    fun addInputLink(link: Link)

    fun addOutputLink(link: Link)

    fun getInputLinks(): List<Link>

    fun getOutputLinks(): List<Link>
}
