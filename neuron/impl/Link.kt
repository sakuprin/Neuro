package edu.sakuprin.neuro.neuron.impl

import edu.sakuprin.neuro.neuron.Neuron

data class Link(var inputNeuron: Neuron? = null, var outputNeuron: Neuron? = null, var weight: Double = 0.toDouble())

