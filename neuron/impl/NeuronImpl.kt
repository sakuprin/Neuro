package edu.sakuprin.neuro.neuron.impl

import edu.sakuprin.neuro.activationFunctions.ActivationFunction
import edu.sakuprin.neuro.neuron.Neuron
import java.util.*

class NeuronImpl : Neuron {
    private val inputNeuralLinks = ArrayList<Link>()
    private val outputNeuralLinks = ArrayList<Link>()
    private var activationFunction: ActivationFunction? = null


    override fun recalculateOutputValue(): Double {
        var calculateValue = 0.0
        for (link in inputNeuralLinks) {
            calculateValue += link.inputNeuron!!.recalculateOutputValue() * link.weight
        }
        calculateValue = activationFunction!!.process(calculateValue)
        return calculateValue
    }

    override fun getDerivativeValue(): Double {
        var sum = 0.0
        for (link in inputNeuralLinks) {
            sum += link.inputNeuron!!.recalculateOutputValue() * link.weight
        }
        return activationFunction!!.recalculate(sum)
    }

    override fun setActivationFunction(activationFunction: ActivationFunction) {
        this.activationFunction = activationFunction
    }

    override fun getInputLinks(): List<Link> {
        return inputNeuralLinks
    }

    override fun getOutputLinks(): List<Link> {
        return outputNeuralLinks
    }

    override fun addInputLink(link: Link) {
        inputNeuralLinks.add(link)
    }

    override fun addOutputLink(link: Link) {
        outputNeuralLinks.add(link)
    }
}