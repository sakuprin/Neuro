package edu.sakuprin.neuro.neuron.impl

import edu.sakuprin.neuro.activationFunctions.ActivationFunction
import edu.sakuprin.neuro.neuron.Neuron
import edu.sakuprin.neuro.neuron.impl.Link
import sun.reflect.generics.reflectiveObjects.NotImplementedException
import java.util.*

 class InputNeuron : Neuron {

    private var inputValue: Double = 0.toDouble()
    private val outputLinks = ArrayList<Link>()
    private var activationFunction: ActivationFunction? = null

    fun setInputValue(value: Double) {
        inputValue = value
    }

    override fun recalculateOutputValue(): Double {
        return inputValue
    }

    override fun getDerivativeValue(): Double {
        return activationFunction!!.recalculate(inputValue)
    }


    override fun setActivationFunction(activationFunction: ActivationFunction) {
        this.activationFunction = activationFunction
    }

    override fun getInputLinks(): List<Link> {
        return emptyList()
    }

    override fun getOutputLinks(): List<Link> {
        return outputLinks
    }

    override fun addOutputLink(link: Link) {
        outputLinks.add(link)
    }

    override fun addInputLink(neuralLink: Link) {
        throw NotImplementedException()
    }

}