package edu.sakuprin.neuro.utils.infix

import edu.sakuprin.neuro.neuralNetwork.impl.NeuralNetworkImpl
import edu.sakuprin.neuro.neuron.Neuron
import edu.sakuprin.neuro.neuron.impl.InputNeuron
import java.util.*

infix fun NeuralNetworkImpl.setInputValues(inputValues: DoubleArray) {
    for (index in inputLayer.indices) {
        (inputLayer[index] as InputNeuron).setInputValue(inputValues[index])
    }
}

infix fun NeuralNetworkImpl.recalculateOutputValues(outputLayer: ArrayList<Neuron>): DoubleArray {
    val result = DoubleArray(outputLayer.size)
    for (i in outputLayer.indices) {
        val neuron = outputLayer[i]
        result[i] = neuron.recalculateOutputValue()
    }
    return result
}