package edu.sakuprin.neuro.utils.infix

import edu.sakuprin.neuro.learning.impl.UniversityImpl
import edu.sakuprin.neuro.neuralNetwork.NeuralNetwork
import edu.sakuprin.neuro.neuralNetwork.impl.NeuralNetworkImpl
import java.text.MessageFormat
import java.util.*

fun UniversityImpl.training(neuralNetwork: NeuralNetwork, inputs: Array<DoubleArray>, expectedOutputs: Array<DoubleArray>): NeuralNetwork {
    for (index in inputs.indices) {
        this.learning(neuralNetwork, inputs[index], expectedOutputs[index])
    }
    return neuralNetwork;
}

fun result(neuralNetwork: NeuralNetwork, inputValues: DoubleArray) {
    val builder = StringBuilder(Arrays.toString(inputValues))
    builder.append(" ").append("=>")
    for (out in neuralNetwork.calculate(inputValues)) {
        builder.append(" ").append(MessageFormat.format("{0}", out))
    }
    println(builder)
}

fun getIntelligentNeuralNetwork(inputCount: Int, middleCount: Int, outputCount: Int, limit: Int): NeuralNetwork {
    val inputs = arrayOf<DoubleArray>(
            doubleArrayOf(0.0, 0.0, 0.0),
            doubleArrayOf(0.0, 1.0, 1.0),
            doubleArrayOf(1.0, 0.0, 0.0),
            doubleArrayOf(1.0, 1.0, 1.0)
    );
    val expectedOutputs = arrayOf<DoubleArray>(
            doubleArrayOf(0.0),
            doubleArrayOf(0.35),
            doubleArrayOf(0.75),
            doubleArrayOf(1.0)
    );
    val teacher = UniversityImpl()
    var network = NeuralNetworkImpl().init(inputCount, middleCount, outputCount)
    for (index in 1..limit) {
        network = teacher.training(network, inputs, expectedOutputs)
    }
    return network
}